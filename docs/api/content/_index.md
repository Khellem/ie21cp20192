# Alimentador Automático para Pets. 

Descrição do Projeto: 
    Trata-se de um alimentador automático para animais domésticos de pequeno porte (podendo - o projeto - ser alterado de acordo com a necessidade), que armazena a ração em um recipiente e libera a quantia desejada, a cada período de tempo (horas) programada, de modo que a ração permaneça sempre limpa e fresca. Evita-se desperdício e eventuais problemas pela contaminação do alimento.


# Lista de Materiais

    1 RC servo - tamanho normal
    1 RC servo (alterado para girar 360°) 
    1 Broca impressa na Impressora 3D
    1 Cano PVC "T" 1/2"
    1 Arduino Uno
    1 Encoder KY-040 
    1 Display LCD HD44780
    1 Botão
    1 Funil de alimentos
    1 Caixa 
    1 Fonte 12V (500Ma)
    2 Capacitores eletrolíticos 25µ

# Resultados 

# Mídias

![alt text](img/ "Imagem 1")

# Esquema Elétrico

# Desafios encontrados

1. A configuração elétrica e a programação do display LCD foram complicadas.


    
    
