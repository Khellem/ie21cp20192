#include <LiquidCrystal.h>
#include <Wire.h>      // needed for the RTC libraty
#include <Time.h>
#include <DS1307RTC.h> // Real Time Clock Library
#include <Servo.h>

Servo agitador;
Servo broca;

LiquidCrystal lcd(12, 11, 5, 8, 7, 6);

//Constantes que definem o numero de cada porta e seu componente:
const int botao = A3;     //Porta do botão
const int pAgitador = 9; //Porta do agitador
const int pBroca = 10; //Porta da broca
const int PinCLK = 2; //Porta dos pino CLK do encoder
const int PinDT = 3;  //Porta do pino DT do encoder       
const int PinSW = 4;  //Porta do pino SW do encoder


//Variáveis:
int botaoPosicao; // variable for reading the pushbutton status
int horaQTD;
int i = 0;
int posPonteiro = 0;
volatile boolean TurnDetected;
volatile boolean up;
int posicaoServo;
static long virtualPosition = 0;
tmElements_t tm;    // This sectionm reads the time from the RTC, sets it in tmElements tm (nice to work with), then displays it.
int menu=1;
//Vetores 
int hora[6];
int minuto[6];
int quantidade[6];


void isr ()  {                    // Interrupt service routine is executed when a HIGH to LOW transition is detected on CLK
 if (digitalRead(PinCLK))         // this keeps an eye out for the rotary encoder being turned regardless of where the program is
   up = digitalRead(PinDT);       // currently exectuting - in other words, during the main loop this ISR will always be active
 else                             // Faz com que possamos girar o encoder mesmo quando o programa esta fazendo
   up = !digitalRead(PinDT);      // outra ação, como a função alimentar
 TurnDetected = true;
}

void setup() {
  lcd.begin(16,2);

  pinMode(botao, INPUT);
   
  pinMode(PinCLK,INPUT);
  pinMode(PinDT,INPUT);  
  pinMode(PinSW,INPUT);
  pinMode(botao, INPUT);
  
  attachInterrupt(0,isr,FALLING);
  
  digitalWrite(PinCLK, HIGH); 
  digitalWrite(PinDT, HIGH);
  digitalWrite(PinSW, HIGH); 

  
  
}

void loop() {
  static long i = 0;
 if(menu == 1)
 { 
  RTC.read(tm); 
  lcd.setCursor(0,0);
  printDigits(tm.Hour);
  lcd.print(":");
  printDigits(tm.Minute);
  lcd.print(":");
  printDigits(tm.Second);
 
  // Escreve a segunda parte do "menu"
  lcd.setCursor(0,2);
  lcd.print("Ver horarios");
  // Checar se a pessoa selecionou a opçao de ver horários então utilizar lcd.clear screen
  
  // Adicionar ponteiro indicando onde a pessoa está mexendo
  lcd.setCursor(0,10);
  posPonteiro = 0;
  lcd.setCursor(14,posPonteiro);
  lcd.print("<-");
 }
 else
 {
  verHorario();
 }
 
  if (TurnDetected){
    if (up){
      lcd.setCursor(14,1);
      lcd.print("  ");
      posPonteiro = 0;
      lcd.setCursor(14,0);
      lcd.print("<-");
      delay(500);
    }
    else{
      lcd.setCursor(14,0);
      lcd.print("  ");
      posPonteiro = 1;
      lcd.setCursor(14,1);
      lcd.print("<-");
      delay(500);  
    }
  }

  if (!(digitalRead(PinSW)) && posPonteiro == 0){
    configurarHora();
  }
  else if (!(digitalRead(PinSW)) && posPonteiro == 1){
    lcd.clear();
    menu = 0;
    verHorario();
    }
  
  //Se o botão for pressionado, ele devolve o valor HIGH
  botaoPosicao = digitalRead(botao);
  if(botaoPosicao == HIGH)
  {
    // Chama a função de alimentação:
    alimentar(3);
  }

  for (i=0; i < horaQTD; i++){
    if (hora[i] == tm.Hour && minuto[i] == tm.Minute){
      alimentar(quantidade[i]);
    }
  }
}


void printDigits(int digits){   // utility function for digital clock display: prints leading 0
  if(digits < 10)
    lcd.print('0');
  lcd.print(digits);
}

void alimentar (int voltas){
  

  agitador.attach(pAgitador);
  for(posicaoServo = 0; posicaoServo <= 180; posicaoServo += 1){                                  
      agitador.write(posicaoServo);              
      delay(5);  
  }
     
  for(posicaoServo = 180; posicaoServo>=0; posicaoServo-=1){                                
      agitador.write(posicaoServo);          
      delay(5);   
   } 
   delay(200);
    
   for(posicaoServo = 0; posicaoServo <= 180; posicaoServo += 1){                                  
      agitador.write(posicaoServo);              
      delay(5);   
    } 
    
   for(posicaoServo = 180; posicaoServo>=0; posicaoServo-=1){                                
       agitador.write(posicaoServo);          
       delay(5);   
   }  
   agitador.detach();


   broca.attach(pBroca);
   //for (int c = 0; c < voltas; c++){
      broca.write(800);  
      delay(600);
      broca.write(80);
      delay(200);
      broca.write(800);  
      delay(600);   
      broca.write(80);  
      delay(200);                     
      //}                           
   broca.detach();
}

void configurarHora(){ 
  lcd.blink();
  lcd.setCursor(0,0);
  delay(500);
  do {
      i = tm.Hour;
      if (TurnDetected){        
        if (up){
          i--;
          if (i < 0)
            i = 23;
      }
      else{
        i++;
        if (i > 23)
          i = 0;
      }
      delay(250); 
      TurnDetected = false;         
    }
    lcd.setCursor(0,0);
    tm.Hour = i;
    RTC.write(tm);
    printDigits(tm.Hour);
  } while ((digitalRead(PinSW)));
  
  delay(500);
  
  do {
    i = tm.Minute;
    if (TurnDetected){        
      if (up){
        i--;
        if (i < 0)
          i = 60;
    }
    else{
      i++;
      if (i > 60)
        i = 0;
    }
    delay(250); 
    TurnDetected = false;         
    }
    lcd.setCursor(3,0);
    tm.Minute = i;
    RTC.write(tm);
    printDigits(tm.Minute);
  } while ((digitalRead(PinSW)));
  
  lcd.noBlink();
  delay(500);
}

void verHorario (){ 
   int l, j=0, k=0;
   int x=11;
   for(l=0; l<4; l++)
   {
     if(l%2==0)
     {
       lcd.setCursor(j, 0);
       lcd.print(x);
       lcd.print(":");
       lcd.print(x);
       j=+6;
     }

     else
     {
       lcd.setCursor(k, 1);
       lcd.print(x);
       lcd.print(":");
       lcd.print(x);
       k=+6;
     }
   }
   delay(3000);
   lcd.clear();
   menu = 1;
   
}
